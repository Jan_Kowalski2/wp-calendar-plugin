<?php

class Timetable_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */

    private $wpdb;

	function __construct() {
		parent::__construct(
			'timetable_widget', // Base ID
			esc_html__( 'Timetable CMS Calendar', 'timetable_widget' ), // Name
			array( 'description' => esc_html__( 'Widget for LearnPress calendar integration for CMS', 'timetable_widget' ), ) // Args
        );
        
        global $wpdb;
        $this->wpdb = &$wpdb;
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
        echo "<h3>Plan zajęć</h3>";
        echo '<ul id="timetableData" class="hidden">';
        echo render_content(get_post_meta_content(false));
        echo '</ul>';
        echo '<section class="calendar-root"><aside class="calendar-plugin small">';
        echo '<div id="timetable" class="vanilla-calendar"></div>';
        echo '<div id="timetable-events" class="calendar-events"></div>';
        echo '</aside>';
        echo '</section>';
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Timetable_Widget

function register_timetable_widget() {
    register_widget( 'Timetable_Widget' );
}
add_action( 'widgets_init', 'register_timetable_widget' );
?>

