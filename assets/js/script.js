"use strict";

const WEEK_DAYS_PL = ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'];

function calendarInit() {
    const DATE_KEYS = ['_lp_time_limit_start', '_lp_time_limit_end'];
    Object.freeze(DATE_KEYS);
    
    const collectedDataGroupedByPost = {};

    const dataRoot = document.getElementById("timetableData");

    if (dataRoot) {
        dataRoot.querySelectorAll("li").forEach(listElement => {
            const isIdAlreadySaved = Object.keys(collectedDataGroupedByPost).includes(listElement.className);
            
            if (!isIdAlreadySaved) {
                collectedDataGroupedByPost[listElement.className] = {}
            }
    
            const key = listElement.querySelector(".post__key").textContent;
            const value = listElement.querySelector(".post__value").textContent;
            const postTitle = listElement.querySelector(".post__title").textContent;
            const postSlug = listElement.querySelector(".post__name").textContent;
            const courseTitle = listElement.querySelector(".post__course-title").textContent;
            const courseSlug = listElement.querySelector(".post__course-name").textContent;
    
            collectedDataGroupedByPost[listElement.className][key] = value;
            collectedDataGroupedByPost[listElement.className].postTitle = postTitle;
            collectedDataGroupedByPost[listElement.className].postSlug = postSlug;
            collectedDataGroupedByPost[listElement.className].courseTitle = courseTitle;
            collectedDataGroupedByPost[listElement.className].courseSlug = courseSlug;
            collectedDataGroupedByPost[listElement.className].id = listElement.className;
        });
    
        const collectedDataGroupedByTime = Object.values(collectedDataGroupedByPost).reduce((acc, currentObj) => {
            const [start, startHour] = currentObj[DATE_KEYS[0]].split(" ");
            const [_, endHour] = currentObj[DATE_KEYS[1]].split(" ");
    
            if (!acc[start]) {
                acc[start] = [];
            }
    
            acc[start] = [
                ...acc[start],
                {
                    ...currentObj,
                    startHour,
                    endHour,
                }
            ];
    
            return acc;
        }, {});
    
        listInit(collectedDataGroupedByTime);
    
        const availableDates = Object.values(collectedDataGroupedByPost).reduce((acc, currentObj) => {
            const start = { date: currentObj[DATE_KEYS[0]].split(" ")[0] };   
            const end = { date: currentObj[DATE_KEYS[1]].split(" ")[0] };   
    
            const startDate = new Date(Date.parse(start.date));
            const endDate = new Date(Date.parse(end.date));
    
            if (startDate === endDate) {
                return [
                    ...acc,
                    start
                ]
            }
    
            return [
                ...acc,
                start,
                end
            ]
        }, []);
    
        new VanillaCalendar({
            selector: "#timetable",
            datesFilter: true,
            months: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
            shortWeekday: ['Ndz', 'Pon', 'Wto', 'Śro', 'Czw', 'Pią', 'Sob'],
            availableDates,
            onSelect: data => {
               showCalendarPopup(data.data.date, collectedDataGroupedByTime);
            }
        });
    }
}

function showCalendarPopup(dateString, groupedByDate) {
    const root = document.getElementById("timetable-events"); 
    const dateAttr = root.getAttribute("date");
    const data = groupedByDate[dateString];

    if (!Array.isArray(data) || data.length === 0) {
        return;
    }

    if (dateAttr === dateString) {
        root.classList.remove("shown");
        root.setAttribute("date", null);
    } else if (dateAttr !== dateString) {
        root.classList.add("shown");
        root.setAttribute("date", dateString);
    }

    root.innerHTML = "";

    const titleElem = document.createElement("strong");
    const dateObj = new Date(Date.parse(dateString));
    const weekDayTitle = WEEK_DAYS_PL[dateObj.getDay()];
    titleElem.textContent = `${dateString} (${weekDayTitle})`;

    root.appendChild(titleElem);

    const dayList = document.createElement("ul"); 

    data.forEach(({ startHour, endHour, postTitle, postSlug, courseTitle, courseSlug }) => {
        const dayListItem = document.createElement("li");
        const hoursElem = document.createElement("p");
        const contentElem = document.createElement("div")
        const linkElem = document.createElement("a");
        const listItemTitleElem = document.createElement("h4");
        const titleSmallElem = document.createElement("small");

        titleSmallElem.textContent = `(${courseTitle})`;

        hoursElem.classList.add("list-item__hours"); 
        contentElem.classList.add("list-item__content");

        hoursElem.textContent = `${startHour} - ${endHour}`
        listItemTitleElem.textContent = postTitle;
        listItemTitleElem.appendChild(titleSmallElem);

        linkElem.href = `/kursy/${courseSlug}/lessons/${postSlug}`;
        linkElem.textContent = `Link do lekcji "${postTitle}"`;
        contentElem.appendChild(listItemTitleElem);
        contentElem.appendChild(linkElem);
        dayListItem.appendChild(hoursElem);
        dayListItem.appendChild(contentElem);
        dayList.appendChild(dayListItem);
    });

    root.appendChild(dayList);
}

function listInit(objectGroupedByDates) {
    const listRoot = document.getElementById("timetableList");

    if (listRoot) {
        Object.entries(objectGroupedByDates).forEach(([ date, values ] ) => {
            const day = document.createElement("li");
            const dayList = document.createElement("ul"); 
            const titleElem = document.createElement("strong"); 
    
            day.appendChild(titleElem);
            day.appendChild(dayList);
            
            values.forEach(({ startHour, endHour, postTitle, postSlug, courseTitle, courseSlug }) => {
                const dayListItem = document.createElement("li");
                const hoursElem = document.createElement("p");
                const contentElem = document.createElement("div")
                const linkElem = document.createElement("a");
                const listItemTitleElem = document.createElement("h4");
                const titleSmallElem = document.createElement("small");

                titleSmallElem.textContent = `(${courseTitle})`;
        
                hoursElem.classList.add("list-item__hours"); 
                contentElem.classList.add("list-item__content");
        
                const dateObj = new Date(Date.parse(date));
                const weekDayTitle = WEEK_DAYS_PL[dateObj.getDay()];
                titleElem.textContent = `${date} (${weekDayTitle})`;
                hoursElem.textContent = `${startHour} - ${endHour}`
                listItemTitleElem.textContent = postTitle;
                listItemTitleElem.appendChild(titleSmallElem);
        
                linkElem.href = `/kursy/${courseSlug}/lessons/${postSlug}`;
                linkElem.textContent = `Link do lekcji "${postTitle}"`;
                contentElem.appendChild(listItemTitleElem);
                contentElem.appendChild(linkElem);
                dayListItem.appendChild(hoursElem);
                dayListItem.appendChild(contentElem);
                dayList.appendChild(dayListItem);
            });
    
            listRoot.appendChild(day);
        });
    }

}

(function() {
    document.addEventListener("DOMContentLoaded", () => {
        calendarInit();
    });
})();
