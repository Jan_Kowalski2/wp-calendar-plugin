<?php
/*
    Plugin Name: Calendar Timetable for extending Learnpress plugin (Group 1, CMS, PJATK)
    Description: A timetable plugin to show calendar in admin panel or as widget for time limited lessons created by LearnPress plugin 
    Author: Piotr Kwiatek, Szymon Wróblewski, Mikołaj Lenart, Hubert Kryśko, Kinga Miga
    Version: 0.6
*/

define( 'LP_TABLE_PREFIX', 'learnpress_' );

add_action('admin_menu', 'calendar_plugin');

function calendar_plugin() {
    add_menu_page( 'Calendar', 'Plan zajęć', 'edit_posts', 'calendar', 'init', 'dashicons-calendar' );
}

define( 'PLUGIN_FILE', __FILE__ );
define( 'PLUGIN_PATH', trailingslashit( plugin_dir_path( PLUGIN_FILE ) ) );

require( PLUGIN_PATH . 'functions.php' );
require( PLUGIN_PATH . 'calendar-widget.php' );

function get_post_meta_content($should_check_user_id) {
    global $wpdb;

    $query = ("SELECT posts_lesson.post_title AS post_title, posts_course.post_title AS course_title, pm.meta_value AS meta_value, pm.meta_key AS meta_key, posts_lesson.post_name AS post_name, posts_course.post_name AS course_name
        FROM wordpress.wp_postmeta pm 
        JOIN wordpress.wp_learnpress_section_items lsi ON pm.post_id = lsi.item_id
        JOIN wordpress.wp_learnpress_sections ls ON lsi.section_id = ls.section_id
        JOIN wordpress.wp_posts posts_course ON ls.section_course_id = posts_course.ID
        JOIN wordpress.wp_posts posts_lesson ON pm.post_id = posts_lesson.ID
        WHERE (pm.meta_key = '_lp_time_limit_start' OR pm.meta_key = '_lp_time_limit_end')");
    if(!current_user_can('administrator') && $should_check_user_id == true ) {
        $query .= " AND posts_lesson.post_author = " . get_current_user_id();
    }

    $posts = $wpdb->get_results($query);

    return $posts;
}

function render_content($post_dates = array()) {
    foreach($post_dates as $single_post) {
        echo '<li class="post-' . $single_post->post_name . $single_post->course_name . '">';
        echo '<span class="post__title">' . $single_post->post_title . '</span>';
        echo '<span class="post__key">' . $single_post->meta_key . '</span>';
        echo '<span class="post__value">' . $single_post->meta_value . '</span>';
        echo '<span class="post__name">' . $single_post->post_name . '</span>';
        echo '<span class="post__course-title">' . $single_post->course_title . '</span>';
        echo '<span class="post__course-name">' . $single_post->course_name . '</span>';
        echo '</li>';
    }
}
 
function init() {
    echo "<h1>Plan zajęć</h1>";
    echo '<ul id="timetableData" class="hidden">';
    echo render_content(get_post_meta_content(true));
    echo '</ul>';
    echo '<section class="calendar-root"><aside class="calendar-plugin">';
    echo '<div id="timetable" class="vanilla-calendar"></div>';
    echo '<div id="timetable-events" class="calendar-events"></div>';
    echo '</aside>';
    echo '<section class="calendar-list"><div class="vanilla-calendar-header"><div class="vanilla-calendar-header__label"><strong>Plan zajęć</strong></div></div><ul id="timetableList"></ul></section>';
    echo '</section>';
}
 
?>

