<?php
    wp_enqueue_style( 'style-calendar-admin', plugin_dir_url( __FILE__ ) . 'assets/css/style.css', false, '1.0', 'all');
    wp_enqueue_style( 'timetable', plugin_dir_url( __FILE__ ) . 'assets/css/vanilla-calendar-min.css', false, '1.0', 'all');
    wp_enqueue_script( 'timetable-cms-group1-plugin-script', plugin_dir_url( __FILE__ ) . 'assets/js/vanilla-calendar-min.js', false, 1.0, true);
    wp_enqueue_script( 'timetable-cms-group1-script', plugin_dir_url( __FILE__ ) . 'assets/js/script.js', false, 1.0, true);
?>